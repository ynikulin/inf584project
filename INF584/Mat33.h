#pragma once
#include "Vec3.h"

template <class T> class Mat33 {
public:
	Mat33(Vec3<T> m0, Vec3<T> m1, Vec3<T> m2) {
		m[0] = m0;
		m[1] = m1;
		m[2] = m2;
	}
	inline Vec3<T> operator* (const Vec3<T> & P) const {
		Vec3<T> res;
		res[0] = dot(m[0], P);
		res[1] = dot(m[1], P);
		res[2] = dot(m[2], P);
		return (res);
	};
protected:
	Vec3<T> m[3];
};
typedef Mat33<float> Mat33f;

/// Matrices are used to convert from one color space to another
const Mat33f RGBtoXYZMat(Vec3f(0.412453, 0.357580, 0.180423),
	Vec3f(0.212671, 0.715160, 0.072169),
	Vec3f(0.019334, 0.119193, 0.950227));

const Mat33f XYZtoRGBMat(Vec3f(3.240479, -1.537150, -0.498535),
	Vec3f(-0.969256, 1.875992, 0.041556),
	Vec3f(0.055648, -0.204043, 1.057311));
