#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

#include "PointLight.h"
#include "Mat33.h"
#include "Mesh.h"

using namespace std;
/// A simple vertex class storing position and normal
void Vertex::initColor(Vec3f cameraPos, Vec3f matDiffuse, Vec3f matSpecular, float matShininess)
{
	color = AMBIENT_IMPACT;

	Vec3f fromPtoCamera = cameraPos - p;
	fromPtoCamera.normalize();

	for (int i = 0; i < LIGHTS_COUNT; ++i)
	{
		PointLight light = LIGHTS[i];
		Vec3f fromPtoLight = light.p - p;
		fromPtoLight.normalize();

		Vec3f reflection = (n * (dot(fromPtoLight, n) * 2)) - fromPtoLight;
		float LdotN = std::max(dot(fromPtoLight, n), 0.0f);
		float RdotV = std::max(dot(reflection, fromPtoCamera), 0.0f);

		color += matDiffuse * light.diffuse * LdotN + matSpecular * light.specular * powf(RdotV, matShininess);
	}
}

Vec3f Vertex::RGBtoXYZ(Vec3f& color) 
{
	return RGBtoXYZMat * color;
}
Vec3f Vertex::XYZtoCIELAB(Vec3f& color) 
{
	Vec3f res;
	if (color[1] / D50WhitePoint[1] > 0.008856)
		res[0] = 116 * pow(color[1] / D50WhitePoint[1], 1.0f / 3) - 16;
	else
		res[0] = 903.3 * color[1] / D50WhitePoint[1];
	res[1] = 500 * (fCIELAB(color[0] / D50WhitePoint[0]) - fCIELAB(color[1] / D50WhitePoint[1]));
	res[2] = 200 * (fCIELAB(color[1] / D50WhitePoint[1]) - fCIELAB(color[2] / D50WhitePoint[2]));
	return res;
}
Vec3f Vertex::RGBtoCIELAB(Vec3f& color) 
{
	return XYZtoCIELAB(RGBtoXYZ(color));
}
Vec3f Vertex::XYZtoRGB(Vec3f& color) 
{
	return XYZtoRGBMat * color;
}
Vec3f Vertex::CIELABtoXYZ(Vec3f& color) 
{
	Vec3f res;
	float P = (color[0] + 16) / 116;

	res[0] = D50WhitePoint[0] * pow((P + color[1] / 500), 3);
	res[1] = D50WhitePoint[1] * P * P * P;
	res[2] = D50WhitePoint[2] * pow((P - color[2] / 200), 3);
	return res;
}
Vec3f Vertex::CIELABtoRGB(Vec3f& color) 
{
	return XYZtoRGB(CIELABtoXYZ(color));
}
float Vertex::fCIELAB(float t)
{
	if (t > 0.008856)
		return pow(t, 1.0f / 3);
	return 7.787 * t + 16 / 116;
}

void Mesh::loadOFF(const string & filename) 
{
	ifstream in(filename.c_str());
	if (!in) {
		cout << "can't open file " << filename << endl;
		exit(EXIT_FAILURE);
	}
	string offString;
	unsigned int sizeV, sizeT, tmp;
	in >> offString >> sizeV >> sizeT >> tmp;
	V.resize(sizeV);
	T.resize(sizeT);
	for (unsigned int i = 0; i < sizeV; i++)
		in >> V[i].p;
	int s;
	for (unsigned int i = 0; i < sizeT; i++) {
		in >> s;
		for (unsigned int j = 0; j < 3; j++)
			in >> T[i].v[j];
	}
	in.close();
	centerAndScaleToUnit();
	recomputeNormals();

	for (unsigned int i = 0; i < V.size(); i++)
		V[i].valency = 0;

	//here we iterate over all teh triangles. Thus, we count twice every point.
	//to neutralize this we only increment a vertex' valaency every time (+1 and not +2).
	for (unsigned int i = 0; i < T.size(); i++)
		for (unsigned int j = 0; j < 3; j++) {
			V[T[i].v[j]].valency++;
		}
	/*for (unsigned int i = 0; i < V.size(); i++)
	printf("valency = %d\n", V[i].valency);*/
}

void Mesh::recomputeNormals() 
{
	for (unsigned int i = 0; i < V.size(); i++)
		V[i].n = Vec3f(0.0, 0.0, 0.0);
	for (unsigned int i = 0; i < T.size(); i++) {
		Vec3f e01 = V[T[i].v[1]].p - V[T[i].v[0]].p;
		Vec3f e02 = V[T[i].v[2]].p - V[T[i].v[0]].p;
		Vec3f n = cross(e01, e02);
		n.normalize();
		for (unsigned int j = 0; j < 3; j++)
			V[T[i].v[j]].n += n;
	}
	for (unsigned int i = 0; i < V.size(); i++)
		V[i].n.normalize();
}

void Mesh::centerAndScaleToUnit() 
{
	Vec3f c;
	for (unsigned int i = 0; i < V.size(); i++)
		c += V[i].p;
	c /= V.size();
	float maxD = dist(V[0].p, c);
	for (unsigned int i = 0; i < V.size(); i++) {
		float m = dist(V[i].p, c);
		if (m > maxD)
			maxD = m;
	}
	for (unsigned int i = 0; i < V.size(); i++)
		V[i].p = (V[i].p - c) / maxD;
}

void Mesh::increase3DContrast() 
{
	if (CIELAB_MODE_ON)
		for (unsigned int i = 0; i < V.size(); i++)
			V[i].color = Vertex::RGBtoCIELAB(V[i].color);

	vector<Vertex> *smoothed = new vector<Vertex>(V.size());
	//we iterate over all the triangles and thus every vertex will be counted twice
	for (unsigned int i = 0; i < T.size(); i++) {
		(*smoothed)[T[i].v[0]].color += (V[T[i].v[1]].color + V[T[i].v[2]].color);
		(*smoothed)[T[i].v[1]].color += (V[T[i].v[0]].color + V[T[i].v[2]].color);
		(*smoothed)[T[i].v[2]].color += (V[T[i].v[1]].color + V[T[i].v[0]].color);
	}
	for (unsigned int i = 0; i < V.size(); i++)
		(*smoothed)[i].color /= (2 * V[i].valency);

	vector<Vertex> *contrast = new vector<Vertex>(V.size());
	for (unsigned int i = 0; i < V.size(); i++)
		(*contrast)[i].color = V[i].color - (*smoothed)[i].color;

	for (unsigned int i = 0; i < V.size(); i++) {
		if (CIELAB_MODE_ON)
		{
			float k = (V[i].color[0] + (GLfloat)lambda*(*contrast)[i].color[0]) / V[i].color[0];
			V[i].color[0] += (GLfloat)lambda*(*contrast)[i].color[0];
			V[i].color[1] *= k;
			V[i].color[2] *= k;
		}
		else
		{
			V[i].color += (GLfloat)lambda*(*contrast)[i].color;
		}
	}

	if (CIELAB_MODE_ON)
		for (unsigned int i = 0; i < V.size(); i++)
			V[i].color = Vertex::CIELABtoRGB(V[i].color);
	delete smoothed, contrast;
}