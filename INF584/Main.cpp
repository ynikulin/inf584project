// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2013 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

#include <fstream>
#include <cstdio>
#include <cstdlib>

#include "Vec3.h"
#include "Camera.h"
#include "Mesh.h"
#include "PointLight.h"

using namespace std;

static GLint window;
static unsigned int SCREENWIDTH = 800;
static unsigned int SCREENHEIGHT = 600;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static unsigned int FPS = 0;
static bool fullScreen = false;
static bool wireframe = false;
PointLight LIGHTS[LIGHTS_COUNT];
bool UNSHARP_MASKING_ON = false;
bool CIELAB_MODE_ON = false;

Mesh mesh;

void printUsage () {
	std::cerr << std::endl
		<< "gMini: a minimal OpenGL/GLUT application" << std::endl
		<< "for 3D graphics." << std::endl
		<< "Author : Tamy Boubekeur" << std::endl << std::endl
		<< "Usage : ./gmini [<file.off>]" << std::endl
		<< "Keyboard commands" << std::endl
		<< "------------------" << std::endl
		<< " u: Toggles Unsharp Maksing" << std::endl
		<< " c: Toggles CIELab color mode when doing unsharp masking" << std::endl
        << " ?: Print help" << std::endl
        << " w: Toggles Wireframe Mode" << std::endl
        << " g: Toggles GPU Shaders" << std::endl 
        << " f: Toggles full screen mode" << std::endl 
        << " <drag>+<left button>: rotate model" << std::endl 
        << " <drag>+<right button>: move model" << std::endl
        << " <drag>+<middle button>: zoom" << std::endl
        << " q, <esc>: Quit" << std::endl << std::endl; 
}

void usage () {
    printUsage ();
    exit (EXIT_FAILURE);
}

void init (const char * modelFilename) {    
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    mesh.loadOFF (modelFilename);

    // Specifies the faces to cull (here the ones pointing away from the camera)
    glCullFace (GL_BACK); 

    // Enables face culling (based on the orientation defined by the CW/CCW enumeration).
    glEnable (GL_CULL_FACE);
    
    // Specify the depth test for the z-buffer
    glDepthFunc (GL_LESS);
    
    // Enable the z-buffer in the rasterization)
    glEnable (GL_DEPTH_TEST); 

    // Background color
    glClearColor (AMBIENT_LIGHT[0], AMBIENT_LIGHT[1], AMBIENT_LIGHT[2], 1.0f);

	// Init lights
	LIGHTS[0] = PointLight(Vec3f(-1000, 1000, -1000), Vec3f(1.0, 1.0, 0.9), Vec3f(1.0, 0.7, 0.7));
}

void draw () {
    // Starts a list of triangles (OpenGL immediat mode)
    glBegin (GL_TRIANGLES);

	// calculate the light at each vertex
    for (int i = 0; i < mesh.T.size (); i++) 
        for (int j = 0; j < 3; j++) {
            Vertex & v = mesh.V[mesh.T[i].v[j]];
            
			Vec3f cameraPos;
			camera.getPos(cameraPos);
			// this light is attached to the camera.
			LIGHTS[0].p = cameraPos;
			v.initColor(cameraPos, mesh.diffuse, mesh.specular, mesh.shininess);
        }

	// unsharp mask the surface
	if (UNSHARP_MASKING_ON)
		mesh.increase3DContrast();

	// display the mesh
	for (int i = 0; i < mesh.T.size(); i++)
		for (int j = 0; j < 3; j++)
		{
			Vertex & v = mesh.V[mesh.T[i].v[j]];

			glColor3f(v.color[0], v.color[1], v.color[2]);
			glNormal3f(v.n[0], v.n[1], v.n[2]);
			glVertex3f(v.p[0], v.p[1], v.p[2]);
		}

    // Ends the triangle drawing
    glEnd (); 
}



void display () {
    // Loads the identity matrix as the current OpenGL matrix
    glLoadIdentity ();

    // Erase the color and z buffers.
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Set up the modelview matrix and the projection matrix from the camera
    camera.apply (); 

    draw ();
    // Ensures any previous OpenGL call has been executed
    glFlush ();
	
    // swap the render buffer and the displayed (screen) one
    glutSwapBuffers (); 
}

// This function is executed in an infinite loop. It updated the window title
// (frame-per-second, model size) and ask for rendering
void idle () {
    static float lastTime = glutGet ((GLenum)GLUT_ELAPSED_TIME);
    static unsigned int counter = 0;
    counter++;
    float currentTime = glutGet ((GLenum)GLUT_ELAPSED_TIME);
    if (currentTime - lastTime >= 1000.0f) {
        FPS = counter;
        counter = 0;
        static char winTitle [128];
        unsigned int numOfTriangles = mesh.T.size ();
        sprintf (winTitle, "Number Of Triangles: %d - FPS: %d", numOfTriangles, FPS);
        glutSetWindowTitle (winTitle);
        lastTime = currentTime;
    }

    // calls the display function
    glutPostRedisplay (); 
}

void key (unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {
    case 'f':
        if (fullScreen) {
            glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
            fullScreen = false;
        } else {
            glutFullScreen ();
            fullScreen = true;
        }      
        break;

    case 'q':
    case 27:
        exit (EXIT_SUCCESS);
        break;
    case 'w':
        glPolygonMode (GL_FRONT_AND_BACK, wireframe ? GL_FILL : GL_LINE);
        wireframe = !wireframe;
        break;

	case 'u':
		UNSHARP_MASKING_ON ^= true;
		break;

	case 'c':
		CIELAB_MODE_ON ^= true;
		break;

    default:
        printUsage ();
        break;
    }
    idle ();
}

void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }
    idle ();
}

void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}

void reshape(int w, int h) {
    camera.resize (w, h);
}

int main (int argc, char ** argv) {
    if (argc > 2) {
        printUsage ();
        exit (EXIT_FAILURE);
    }
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("Rendering");
    init (argc == 2 ? argv[1] : "../models/killeroo.off");

    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);   
    glutMainLoop ();
    return EXIT_SUCCESS;
}
