#pragma once

#include <vector>
#include <glut.h>
#include "Vec3.h"

#define lambda 1.5

// Unsharp masking added toggles
extern bool UNSHARP_MASKING_ON;
extern bool CIELAB_MODE_ON;

/// A simple vertex class storing position and normal
class Vertex {

public:
	inline Vertex() {}
	inline Vertex(const Vec3f & p, const Vec3f & n) : p(p), n(n) {}
	inline virtual ~Vertex() {}

	Vec3f p;
	Vec3f n;
	Vec3f color;
	int valency;

	void initColor(Vec3f cameraPos, Vec3f matDiffuse, Vec3f matSpecular, float matShininess);

	/// some functions to convert the color from one space to another
		static Vec3f RGBtoXYZ(Vec3f& color);
		static Vec3f XYZtoCIELAB(Vec3f& color);
		static Vec3f RGBtoCIELAB(Vec3f& color);
		static Vec3f XYZtoRGB(Vec3f& color);
		static Vec3f CIELABtoXYZ(Vec3f& color);
		static Vec3f CIELABtoRGB(Vec3f& color);
		static float fCIELAB(float t);
};

/// A Triangle class expressed as a triplet of indices (over an external vertex list)
class Triangle {
public:
	inline Triangle() {
		v[0] = v[1] = v[2] = 0;
	}
	inline Triangle(const Triangle & t) {
		v[0] = t.v[0];
		v[1] = t.v[1];
		v[2] = t.v[2];
	}
	inline Triangle(unsigned int v0, unsigned int v1, unsigned int v2) {
		v[0] = v0;
		v[1] = v1;
		v[2] = v2;
	}
	inline virtual ~Triangle() {}
	inline Triangle & operator= (const Triangle & t) {
		v[0] = t.v[0];
		v[1] = t.v[1];
		v[2] = t.v[2];
		return (*this);
	}
	unsigned int v[3];
};

/// A Mesh class, storing a list of vertices and a list of triangles indexed over it.
class Mesh {
public:
	std::vector<Vertex> V;
	std::vector<Triangle> T;

	Vec3f diffuse = Vec3f(0.5, 0.5, 0.5);
	Vec3f specular = Vec3f(1, 1, 1);
	float shininess = 2.0f;

	/// Loads the mesh from a <file>.off
	void loadOFF(const std::string & filename);

	/// Compute smooth per-vertex normals
	void recomputeNormals();

	/// scale to the unit cube and center at original
	void centerAndScaleToUnit();

	/// Implementation of 3D unsharp masking
	void increase3DContrast();
};