#pragma once
#include "Vec3.h"
class PointLight {
public:
	inline PointLight() {}
	inline PointLight(const Vec3f& p, const Vec3f& d, const Vec3f& s) : p(p), diffuse(d), specular(s) {};
	Vec3f p;
	Vec3f diffuse;
	Vec3f specular;
};

/// Scene light data
static const int LIGHTS_COUNT = 1;
extern PointLight LIGHTS[LIGHTS_COUNT];
static const Vec3f AMBIENT_LIGHT = Vec3f(0.5, 0.5, 0.5);
static const Vec3f AMBIENT_REFLECTION = Vec3f(0.5, 0.5, 0.5);
static const Vec3f AMBIENT_IMPACT = AMBIENT_LIGHT * AMBIENT_REFLECTION;

/// Constant of the white color used in conversion
const Vec3f D50WhitePoint(0.9642, 1.0000, 0.8249);